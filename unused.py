

@bot.command(description='抓取萌典字型並輸出成emoji')
async def word(ctx, content):
    guild = discord.utils.get(bot.guilds, id=618063341340065822)
    url = 'https://www.moedict.tw/{0}.png'
    words = list(content)
    emoji_list = []
    for i, w in enumerate(words):
        if w not in '!@#$%^&*()_-=+[]{}\\|/'"<>?`~":
            pic = url.format(w)
            # print(pic)

            # https://stackoverflow.com/questions/33101935/

            response = requests.get(pic)
            from io import BytesIO
            from PIL import Image
            img = Image.open(BytesIO(response.content))
            imgbytearr = BytesIO()
            img.save(imgbytearr, format='PNG')
            imgbytearr = imgbytearr.getvalue()

            i = "%03d" % i
            try:
                emoji = await guild.create_custom_emoji(name=i, image=imgbytearr)
            except discord.HTTPException:
                for i in emoji_list:
                    await i.delete()
                return
            emoji_list.append(emoji)
    emoji_list_str = ''
    for i in emoji_list:
        emoji_list_str += str(i)
    await ctx.send(emoji_list_str)
    for i in emoji_list:
        await i.delete()


@bot.command(description='假造文字截圖')
async def fake(ctx, member: discord.Member, content):
    # user = discord.utils.get(bot.get_all_members(), mention=member)  # 尋找該member
    time = datetime.datetime.now().strftime('%I{0}%M{1}').format('點', '分').strip('0')
    if datetime.datetime.now().strftime('%p') == 'AM':
        noon = '今天上午'
    else:
        noon = '今天晚上'
    avatar_url = str(member.avatar_url).replace('webp?size=1024', 'png')
    color = member.colour
    if str(color) == '#000000':  # 如果無設定顏色，discord.colour返回值0
        color = '#dcddde'  # 預設白
    result = fakequote.fq(avatar_url, member.display_name, noon + time, content, color=color)
    await ctx.send(file=discord.File(result))

from discord.ext.commands import has_permissions
class Cog(commands.Cog, command_attrs=dict(checks=(has_permissions(administrator=True)))):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.has_permissions()
    async def foo(self, ctx):
        print('bar')