#!/usr/bin/python3.7
# coding=utf-8
import asyncio
import sys

import discord
from discord.ext.commands import has_permissions
from discord.utils import *
# import logging
import time
"""
logger = logging.getLogger('discord')
logger.setLevel(logging.WARNING)  # 改WARNING
handler = logging.FileHandler(filename='afat_return.log', encoding='utf-8', mode='w')
handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
logger.addHandler(handler)
"""
# from datetime import *
import datetime
import json
from discord.ext import commands

# package import
# from package.fake_quote import fakequote

command_prefix = 'r>'
description = '阿肥重返榮耀，阿肥Return！'
bot = commands.Bot(command_prefix=command_prefix, description=description)

global spline
spline = '-' * 80  # 分隔線


@bot.event  # 初始預備
async def on_ready():
    print('已登入為  : ')
    print('    使用者名稱: ' + bot.user.name)
    print('    使用者ID  : ' + str(bot.user.id))
    print('    使用者全名: ' + str(bot.user))
    print('    MentionID : ' + bot.user.mention)
    print('    顯示名稱  : ' + bot.user.display_name)
    global own
    own = await bot.application_info()
    own = own.owner
    print('    擁有者    : ' + str(own))
    print('    指令前輟  : ' + command_prefix)
    print(spline)
    print('邀請連結  : ')
    print('https://discordapp.com/oauth2/authorize?client_id=' + str(bot.user.id) + '&scope=bot&permissions=8')
    print(spline)
    # default()
    start_up()
    bot.remove_command('help')  # 移除指令
    bot.add_cog(Voting(bot))
    bot.add_cog(temp_voice_channel(bot))
    # bot.add_cog(self_delete_man(bot))  # 自刪俠
    await Voting(bot)  # await 放最後


def start_up():
    global start_channel, main_server, push_emoji, boo_emoji, boo_log_channel, vote_channel, vote_msg_channel
    start_channel = bot.get_channel(parse_data('temp_voice_channel'))  # 由ID獲取暫時語音頻道
    main_server = bot.get_guild(parse_data('main_guild'))  # 由ID獲取主要伺服器
    # push_emoji = discord.utils.get(main_server.emojis, name='Push')
    # boo_emoji = discord.utils.get(main_server.emojis, name='Boo')
    boo_log_channel = bot.get_channel(parse_data('boo_log_channel'))
    vote_channel = bot.get_channel(parse_data('vote_channel'))
    vote_msg_channel = bot.get_channel(parse_data('vote_msg_channel'))


def default():
    data = [
        ('temp_voice_channel', 639063763399737364),
        ('main_guild', 638563200949813263),
        ('boo_log_channel', 639101608378236929),
        ('vote_channel', 638565568756318208),
        ('vote_msg_channel', 639139284875476992)
    ]
    for i in data:
        save_data(i[0], i[1])



class Voting(commands.Cog, command_attrs=dict(hidden=True, checks=(has_permissions(administrator=True)))):
    def __init__(self, bot):
        self.bot = bot

    def __await__(self):
        return self.autovotesetup().__await__()

    @commands.command()  # 測試權限
    @commands.has_permissions()
    async def foo(self, ctx):
        print('bar')

    def vote_data_save(self, message_id, vote_list, voted_members):
        data = {
            'message': message_id,
            'list': vote_list,
            'voted_members': voted_members
        }
        save_data('vote_data', data)

    def vote_data_load(self):
        data = parse_data('vote_data')
        global vote_list, voted_members
        vote_list = data['list']
        voted_members = data['voted_members']

    # discord.utils.get(bot.cached_messages, id=parse_data('vote_data_save')['message'])

    @commands.command(description='投票')  # 測試用
    async def setupvote(self, ctx):
        global vote_msg, vote_list, voted_members
        vote_list = []
        voted_members = []
        self.vote_data_save(None, vote_list, voted_members)
        await self.autovotesetup()
        # ctx.delete()

    async def autovotesetup(self):
        global vote_msg, vote_list, voted_members
        embed = self.edit_embed()
        message_content = """
    >>> 大廳名稱投票！\n每到中午12點跟晚上12點會結算\n並更改大廳名字\n**直接點號碼的表情符號可以投票**\n想新增新選項可以輸入`r>addvote`加你想要的名稱\n例如：`r>addvote 低能兒`\n最多10個選項ㄛ
            """
        vote_msg = await vote_msg_channel.send(content=message_content, embed=embed)
        self.vote_data_load()
        self.vote_data_save(vote_msg.id, vote_list, voted_members)
        '''
        try:
            self.vote_data_save(vote_msg.id, vote_list, voted_members)
        except NameError:
            vote_list = []
            voted_members = []
            self.vote_data_save(vote_msg.id, vote_list, voted_members)
        '''
        try:
            for emoji in number_emojis[:10]:
                await vote_msg.add_reaction(emoji)
        except discord.errors.NotFound:
            pass
        if time.strftime("%p", time.localtime()) == 'AM':
            t = '120000'
        else:
            t = '000000'
        while time.strftime("%H%M%S", time.localtime()) != t:
            await asyncio.sleep(0.1)
        # await asyncio.sleep(300)
        await self.voteend()

    @commands.command(description='投票')  # 測試用
    async def vote(self, ctx, number):
        global vote_list, vote_msg, voted_members
        if ctx.author.id not in voted_members:
            number = int(number)
            if len(vote_list) >= number:
                vote_list[number - 1][0] += 1
            await self.vote_update(ctx.author.id)

    @commands.command(description='投票')  # 測試用
    async def addvote(self, ctx, *, name):
        global vote_list, vote_msg, voted_members
        print()
        if ctx.author.id not in voted_members and len(vote_list) < 11:
            vote_list.append([1, name, ctx.author.id])  # [票數, 名稱, 作者]
            await self.vote_update(ctx.author.id)

    @commands.command(description='投票')  # 測試用
    async def end(self, ctx):
        await self.voteend()

    async def voteend(self):
        global vote_list, vote_msg, voted_members
        vote_list.sort(key=lambda x: x[0], reverse=True)  # 記得要 reverse=True 才會由大到小...
        self.vote_data_save(vote_msg.id, vote_list, voted_members)
        # await vote_msg.delete()
        await vote_msg.clear_reactions()
        await vote_msg.edit(content=None, embed=self.edit_end_embed())  # 把投票結果存下來

        try:
            name = vote_list[0][1]
            if len(name) > 96:
                name = name[0:95]
            try:
                name = escape_markdown(name)  # 空格置換成底線
                submit_user = check_user(vote_list[0][2])
                await vote_channel.edit(name='【大廳】' + name,
                                        topic='【大廳】名稱由 {0} ({1}) 提供'.format(escape_markdown(submit_user.display_name), escape_markdown(str(submit_user))))
            except:
                await vote_channel.edit(name='【大廳】' + name, topic='【大廳】名稱由 unknown 提供')
            vote_list = []
            voted_members = []
            self.vote_data_save(vote_msg.id, vote_list, voted_members)
        except IndexError:  # 無選項時再等一迴圈
            with open('ky.txt', 'r', encoding='utf8') as f:
                from random import choice
                name = choice(f.readlines())
                await vote_channel.edit(name='【大廳】' + name,
                                        topic='【大廳】名稱由 韓國瑜 提供 - 來自《維基語錄‧韓國瑜》')
        await self.autovotesetup()

    async def vote_update(self, id):
        global vote_list, vote_msg, voted_members
        voted_members.append(id)
        self.vote_data_save(vote_msg.id, vote_list, voted_members)
        await vote_msg.edit(content=vote_msg.content, embed=self.edit_embed())

    @commands.command(description='投票')  # 測試用
    async def votelist(self, ctx):
        print(parse_data('vote_data')('list'))

    def edit_embed(self):
        embed = discord.Embed(color=0xFF35A1, timestamp=datetime.datetime.utcnow())
        # try:
        list = parse_data('vote_data')['list']
        for i, data in enumerate(list):
            submit_user = check_user(data[2])
            embed.add_field(name=number_emojis[i] + ' ' + data[1],
                            value=str(data[0]) + ' 票 - by {0}({1})'.format(escape_markdown(submit_user.display_name),
                                                                           escape_markdown(str(submit_user))),
                            inline=False)
        return embed

    def edit_end_embed(self):
        embed = discord.Embed(color=0xFF35A1, timestamp=datetime.datetime.utcnow())
        # try:
        list = parse_data('vote_data')['list']
        for i, data in enumerate(list):
            submit_user = check_user(data[2])
            name = f'**{data[1]}**' if i == 0 else data[1]
            embed.add_field(name=number_emojis[i] + ' ' + name,
                            value=str(data[0]) + ' 票 - by {0}({1})'.format(escape_markdown(submit_user.display_name),
                                                                           escape_markdown(str(submit_user))),
                            inline=False)
        return embed


    @commands.Cog.listener()
    async def on_reaction_add(self, reaction, user):
        global vote_list, vote_msg, voted_members
        msg = reaction.message

        if reaction.count == 10:
            if reaction.emoji is boo_emoji:
                now = datetime.datetime.now() - datetime.timedelta(hours=8)
                embed = discord.Embed(description='點連結跳轉\n'+msg.jump_url, color=0xEE4BB5, timestamp=now)
                embed.set_author(name=msg.author.display_name+' 被噓到摺疊了！', icon_url=msg.author.avatar_url)
                await boo_log_channel.send(embed=embed)

        if vote_msg is not None and reaction.emoji in number_emojis:  # 反應投票
            if vote_msg.id == msg.id:  # 確定投票訊息
                number = number_emojis.index(reaction.emoji)  # 取反應的號碼
                if user.bot is False:  # 排除bot
                    if number > len(vote_list) - 1:
                        await msg.remove_reaction(reaction.emoji, user)
                    elif user.id not in voted_members:
                        number = int(number)
                        if len(vote_list) >= number:
                            vote_list[number][0] += 1
                        await self.vote_update(user.id)
                    else:
                        await msg.remove_reaction(reaction.emoji, user)


@bot.command(description='default')  # 測試用
async def reset_default(ctx):
    default()


def permission_check(member, channel):  # 檢查權限
    permission = channel.permissions_for(member)
    return permission.administrator
    # permission_check(ctx.message.author, ctx.message.channel)


def check_user(member):
    server = main_server
    if type(member) is int:
        member = discord.utils.find(lambda x: x.id == member, server.members)
    else:
        member = discord.utils.find(lambda x: x.mention == member, server.members)
    return member


def save_data(data_name, data_value):  # 使用json存檔
    try:  # 檢查有無檔案
        f = open('data.json', 'r')
        data = json.load(f)
        f.close()
    except (FileNotFoundError, json.JSONDecodeError):
        data = json.loads('{}')
    # if data_value.isdigit():  # 全數字轉換成int
    # data_value = int(data_value)
    data[data_name] = data_value
    with open('data.json', 'w') as f:
        json.dump(data, f)


def parse_data(data_name = None):  # 查找json
    with open('data.json', 'r') as f:
        data = json.load(f)
        if data_name is None:  # 無指定返回全部資料
            return data
        else:
            return data[data_name]


@bot.command(description='test')  # 測試用
async def save(ctx, name, value):
    save_data(name, value)


@bot.command(description='test')  # 測試用
async def read(ctx):
    c = json.dumps(parse_data(), indent=4, sort_keys=True)
    await ctx.send('```json\n{0}```'.format(c))


@bot.command(description='log')  # 查看記錄檔
async def log(ctx):
    pass
    '''
    global logfile
    logfile.close()
    with open('output.log', mode='r', encoding='UTF8') as l:
        await ctx.send(f'```{l.read()}```')
    logfile = open('output.log', 'a', encoding='UTF8')
    sys.stderr = logfile
    '''


@bot.command(description='pong!')  # 查看延遲
async def ping(ctx):
    delay = '{:.0f}'.format(bot.latency*1000)
    await ctx.send('`{0}ms` Pong!'.format(str(delay)))


prefix_whitelist = ('r>', '><', '>', '.', '!')  # str.startswith()接受tuple
number_emojis = ['1⃣', '2⃣', '3⃣', '4⃣', '5⃣', '6⃣', '7⃣', '8⃣', '9⃣', '🔟', '🔄']


@bot.command(description='傳送訊息到ID指定頻道')
async def send(ctx, channel_id, content):
    channel = bot.get_channel(int(channel_id))  # 由ID獲取文字頻道
    await channel.send(content)


@bot.event
async def on_message(msg):
    await bot.process_commands(msg)
    '''
    if not msg.author.bot:
        if len(msg.attachments) > 0:
            await msg.add_reaction(push_emoji)
            await msg.add_reaction(boo_emoji)
    '''

class self_delete_man(commands.Cog):
    @commands.Cog.listener()
    async def on_message_delete(self, msg):
        if not msg.author.bot and not msg.content.startswith(prefix_whitelist):  # 排除以指令前輟開頭及機器人的訊息
            # 自刪俠
            now = datetime.datetime.now() - datetime.timedelta(hours=8)
            delay = now - msg.created_at
            delay = delay.total_seconds()
            delay = "%.3f" % float(str(delay))
            if float(delay) < 15:
                embed = discord.Embed(description=msg.content, color=0xEE4BB5, timestamp=now)
                embed.set_author(name=msg.author.display_name, icon_url=msg.author.avatar_url)
                if len(msg.attachments) == 1:  # 圖片只有一張就直接顯示
                    embed.set_image(url=msg.attachments[0].proxy_url)
                else:
                    for i, l in enumerate(msg.attachments):
                        i += 1
                        embed.add_field(name="圖 " + str(i), value=l.proxy_url)
                await msg.channel.send(content='自刪俠 **{0}** 在 **{1}** 秒內自刪ㄌ！'.format(msg.author.mention, delay),
                                       embed=embed)

@bot.command(pass_context=True)
@commands.has_permissions(administrator=True)
async def voice(ctx, set):
    if set == 'on':
        bot.add_cog(temp_voice_channel(bot))
    elif set == 'off':
        bot.remove_cog('temp_voice_channel')



class temp_voice_channel(commands.Cog):
    @commands.Cog.listener()
    async def on_voice_state_update(self, member, before, after):
        try:
            if after.channel == start_channel:
                new_channel = await start_channel.guild.create_voice_channel(member.display_name,
                                                                             category=start_channel.category)
                #  創立新頻道，預設以使用者為名
                await member.move_to(new_channel)  # 將使用者移過去
                await new_channel.set_permissions(member, manage_channels=True, priority_speaker=True, mute_members=True,
                                                  deafen_members=True, move_members=True, manage_roles=True)  # 新增權限
            if (before.channel != start_channel
                    and before.channel.category == start_channel.category and before.channel.members == []):
                # 判斷離開的頻道是否沒人，而且是暫時頻道
                await before.channel.delete()
        except AttributeError:
            pass


@bot.command(pass_context=True)
@commands.has_permissions(administrator=True)
async def close(ctx):
    await bot.close()


if __name__ == '__main__':  # 開啟bot
    with open('token', 'r') as t:  # 讀取密鑰檔案
        token = t.readline()
    # logfile = open('output.log', 'a', encoding='UTF8')
    # sys.stderr = logfile
    bot.run(token)
